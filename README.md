Simultaneous TCP Connection
===========================

TCP can support client to client connections. This is an example of serverless architecture. :^)

Just build the program with `make` and set two terminals to loop restarts of it with opposite port numbers. Eventually, they'll connect.

    # Terminal #1
    while [[ true ]]
    do
      ./client.out 4051 4050
    done
    
    # Terminal #2
    while [[ true ]]
    do
      ./client.out 4050 4051
    done