#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, char ** argv) {
  char * source_port;
  char * destination_port;
  int status;
  struct addrinfo hints;

  if (argc != 3) {
    printf("Usage: port1 port2\n");
    return 1;
  }

  pid_t master_pid = getpid();

  source_port = argv[1];
  destination_port = argv[2];

  printf("%d: %s -> %s\n", getpid(), source_port, destination_port);

  // Get source information.
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  struct addrinfo * source_info;
  if ((status = getaddrinfo("localhost", source_port, &hints, &source_info)) != 0) {
    printf("%s\n", gai_strerror(status));
    return 1;
  }

  // Get destination information.
  memset(&hints, 0, sizeof hints);
  hints.ai_family = source_info->ai_family;
  hints.ai_socktype = SOCK_STREAM;
  struct addrinfo * destination_info;
  if ((status = getaddrinfo("localhost", destination_port, &hints, &destination_info)) != 0) {
    printf("%s\n", gai_strerror(status));
    return 2;
  }

  int sock;
  if ((sock = socket(source_info->ai_family, source_info->ai_socktype, source_info->ai_protocol)) == -1) {
    perror("SOCKET-BUILD");
    return -1;
  }
  if ((status = bind(sock, source_info->ai_addr, source_info->ai_addrlen)) != 0) {
    perror("SOCKET-BIND");
    return -1;
  }
  printf("%d: BOUND\n", getpid());
  printf("%d: CONNECTING\n", getpid());
  if ((status = connect(sock, destination_info->ai_addr, destination_info->ai_addrlen)) != 0) {
    perror("SOCKET-CONNECT");
    return -1;
  }

  printf("Connected: %d\n", master_pid);

  freeaddrinfo(source_info);
  freeaddrinfo(destination_info);

  // Start a thread to read the connection.
  pid_t writer_child;
  unsigned int send_count = 3;
  if ((writer_child = fork()) == 0) {
    for (int i = 0; i < send_count; i++) {
      char message[] = "How are you?";
      char * message_start = message;
      size_t bytes_left = (sizeof message) - 1;
      while (bytes_left != 0) {
	size_t bytes_sent = send(sock, message_start, bytes_left, 0);
	bytes_left -= bytes_sent;
	message_start += bytes_sent;
      }
    }
    close(sock);
  }

  // Start a thread to write the connection.
  pid_t reader_child;
  if ((reader_child = fork()) == 0) {
    char storage[32];
    int bytes_read;
    while ((bytes_read = recv(sock, storage, (sizeof storage) - 1, 0)) != 0) {
      if (bytes_read == -1) {
	continue;
      }
      storage[bytes_read] = '\0';
      printf("RECIEVED %d: %s\n", master_pid, storage);
    }
  }

  printf("WAITING\n");
  int wstatus;
  waitpid(writer_child, &wstatus, 0);
  waitpid(reader_child, &wstatus, 0);
  printf("DONE\n");
  
  return 0;
}
	      
